<?php

namespace Openapi\Epay\Payments;

use Openapi\Epay\EpayComponent;
use Openapi\Epay\Types\InvoiceType;

/**
 * WebPayment
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class WebPayment extends EpayComponent {

    /**
     * @var array
     */
    private $invoice = [];

    /**
     * @var string
     */
    private $encoded;

    /**
     * @var string
     */
    private $checksum;

    /**
     * Generate invoice fields 
     * 
     * @param \Openapi\Epay\Types\InvoiceType
     */
    public function generateInvoice(InvoiceType $invoice) {

        $this->invoice = $invoice->toArray();

        //add the required service fields to request payload

        $this->invoice['MIN'] = $this->getClientId();

        $this->invoice['EMAIL'] = $this->getClientEmail();

        $this->invoice['EXP_TIME'] = $this->getExpireTime();

        $this->invoice['ENCODING'] = $this->getEncoding();
    }

    /**
     * @return string base64encoded invoice data
     */
    public function getEncoded() {

        if (!empty($this->encoded)) {

            return $this->encoded;
        }

        $data_array = array_keys($this->invoice);

        array_walk($data_array, function(&$value, $key, $values) {

            $value = !empty($values[$key]) ? $value . '=' . $values[$key] : false;
        }, array_values($this->invoice));

        $imploded = implode(PHP_EOL, array_filter($data_array));

        $this->encoded = base64_encode($imploded);

        return $this->encoded;
    }

    /**
     * @return string HASH the encoded string with client secret
     */
    public function getChecksum() {

        if (!empty($this->checksum)) {

            return $this->checksum;
        }

        $this->checksum = $this->hmac($this->getEncoded(), $this->getSecret());

        return $this->checksum;
    }

    /**
     * Gets the fields needed to render the HTML form
     * 
     * @return array
     */
    public function getInvoiceFields() {

        $fields = [
            'PAGE' => $this->getPaymentType(),
            'ENCODED' => $this->getEncoded(),
            'CHECKSUM' => $this->getChecksum(),
            'URL_OK' => $this->getReturnUrl(),
            'URL_CANCEL' => $this->getCancelUrl()
        ];

        return $fields;
    }

}
