<?php

namespace Openapi\Epay\Enums;

/**
 * NotificationsEnum
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class NotificationsEnum {

    /**
     * @var string
     */
    const STATUS_PAID = 'PAID';

    /**
     * @var string
     */
    const STATUS_DENIED = 'DENIED';

    /**
     * @var string
     */
    const STATUS_EXPIRED = 'EXPIRED';

    /**
     * @var string
     */
    const STATUS_OK = 'OK';

    /**
     * @var string
     */
    const STATUS_ERROR = 'ERROR';

    /**
     * @var string 
     */
    const RESPONSE_STATUS_NOT_FOUND = 'NO';

    /**
     * @var string 
     */
    const RESPONSE_STATUS_OK = 'OK';

    /**
     * @var string 
     */
    const RESPONSE_STATUS_ERROR = 'ERR';

}
