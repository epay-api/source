<?php

namespace Openapi\Epay\Types;

use Openapi\Epay\Enums\NotificationsEnum;

/**
 * ResponseType
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class ResponseType {

    /**
     * @var int
     */
    private $INVOICE;

    /**
     * @var string
     */
    private $STATUS;

    /**
     * @var int
     */
    private $PAY_TIME;

    /**
     * @var int
     */
    private $STAN;

    /**
     * @var int
     */
    private $BCODE;

    /**
     * @var float
     */
    private $AMOUNT;

    /**
     * @var string
     */
    private $BIN;

    public function __construct($decoded = []) {

        foreach ($decoded as $key => $value) {
            if (!property_exists(get_class(), $key)) {
                continue;
            }
            $this->{$key} = $value;
        }
    }

    /**
     * @return int
     */
    public function getInvoiceNumber() {
        return $this->INVOICE;
    }

    /**
     * @return string Payment status [PAID | DENIED | EXPIRED]
     */
    public function getStatus() {
        return $this->STATUS;
    }

    /**
     * @return string Payment date time
     */
    public function getPayTime() {

        return $this->PAY_TIME;
    }

    /**
     * @return string BORIKA authorization code 6 numbers/letters  (not empty only when credit cart payment)
     */
    public function getBorikaCode() {

        return $this->BCODE;
    }

    /**
     * @return float (not empty only when there is discount)
     */
    public function getAmount() {

        return $this->AMOUNT;
    }

    /**
     * @return string Card BIN (not empty only when there is discount)
     */
    public function getCardBin() {

        return $this->BIN;
    }

    /**
     * @return int 6 numbers (not empty only when credit cart payment)
     */
    public function getTransactionNumber() {

        return $this->STAN;
    }

    /**
     * @return bool true if invoice is paid
     */
    public function isPaid() {
        return $this->getStatus() == NotificationsEnum::STATUS_PAID ? true : false;
    }

    /**
     * @return bool true if invoice is canceled
     */
    public function isCanceled() {
        return $this->getStatus() == NotificationsEnum::STATUS_DENIED ? true : false;
    }

    /**
     * @return bool true if invoice is canceled
     */
    public function isExpired() {
        return $this->getStatus() == NotificationsEnum::STATUS_EXPIRED ? true : false;
    }

    /**
     * @return string if Invoice was not found into system
     */
    public function returnNoFound() {

        return "INVOICE={$this->getInvoiceNumber()}:STATUS=" . NotificationsEnum::RESPONSE_STATUS_NOT_FOUND;
    }

    /**
     * @param string $message error message 
     * @return string Invoice error
     */
    public function returnError($message = '') {

        $response = [
            'STATUS=' . NotificationsEnum::RESPONSE_STATUS_ERROR
        ];

        !empty($this->getInvoiceNumber()) ?
                        $response[] = 'INVOICE=' . $this->getInvoiceNumber() :
                        '';
        !empty($message) ?
                        $response[] = NotificationsEnum::RESPONSE_STATUS_ERROR . '=' . $message :
                        '';


        return implode(':', $response);
    }

    /**
     * @return string if Invoice OK
     */
    public function returnSuccess() {

        return "INVOICE={$this->getInvoiceNumber()}:STATUS=" . NotificationsEnum::RESPONSE_STATUS_OK;
    }

}
