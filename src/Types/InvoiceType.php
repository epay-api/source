<?php

namespace Openapi\Epay\Types;

/**
 * InvoiceType
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class InvoiceType {

    /**
     * @var string 100 chars max
     */
    var $description;

    /**
     * @var float
     */
    var $amount;

    /**
     * @var string
     */
    var $currency = 'BGN';

    /**
     * @var string
     */
    private $default_currency = 'BGN';

    /**
     * @var integer
     */
    var $invoice_number;

    /**
     * @param array $params
     */
    public function __construct($params = []) {

        foreach ($params as $key => $value) {
            if (!property_exists(get_class(), $key)) {
                continue;
            }
            $this->{$key} = $value;
        }
    }

    /**
     * @return string Description 100 chars max
     */
    public function getDescription() {

        return \substr(trim($this->description), 0, 100);
    }

    /**
     * @return float
     */
    public function getAmount() {

        return (float) $this->amount;
    }

    /**
     * @return float
     */
    public function getCurrency() {

        return !empty($this->currency) ? $this->currency : $this->default_currency;
    }

    /**
     * @return int Invoice number. If was not set, the Invoice number is automatically generated 
     */
    public function getInvoiceNUmber() {

        if (!empty($this->invoice_number)) {

            return (int) $this->invoice_number;
        }

        $invoice = sprintf("%.0f", rand(0, 999999999));

        $this->invoice_number = (int) $invoice;

        return $this->invoice_number;
    }

    /**
     * @return array
     */
    public function toArray() {

        return [
            'INVOICE' => $this->getInvoiceNUmber(),
            'AMOUNT' => $this->getAmount(),
            'CURRENCY' => $this->getCurrency(),
            'DESCR' => $this->getDescription()
        ];
    }

}
