<?php

namespace Openapi\Epay\Notifications;

use Openapi\Epay\EpayComponent;
use Openapi\Epay\Types\ResponseType;

/**
 * EpayNotification Receive payment notifications 
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class EpayNotification extends EpayComponent {

    /**
     * @var array POST data from ePay
     */
    private $post = [];

    /**
     * @var string
     */
    private $encoded;

    /**
     * @var string
     */
    private $checksum;

    /**
     * 
     * @return array ePay post data
     */
    public function getPostData() {

        if (!empty($this->post)) {

            return $this->post;
        }
        
        $this->getNotificationDebug()->write('request');

        $this->getNotificationDebug()->write($_REQUEST);

        $input = \file_get_contents('php://input');

        \parse_str($input, $post);

        $this->post = $post;

        $this->getNotificationDebug()->write('post');

        $this->getNotificationDebug()->write($post);

        return $this->post;
    }

    /**
     * @return string encoded string from post
     */
    public function getEncoded() {

        if (!empty($this->encoded)) {

            return $this->encoded;
        }

        $post = $this->getPostData();

        $encoded = !empty($post['encoded']) ? $post['encoded'] : false;

        $this->encoded = $encoded;

        $this->getNotificationDebug()->write('encoded');

        $this->getNotificationDebug()->write($encoded);

        return $this->encoded;
    }

    /**
     * @return string Checksum string from post
     */
    public function getChecksum() {

        if (!empty($this->checksum)) {

            return $this->checksum;
        }

        $post = $this->getPostData();

        $checksum = !empty($post['checksum']) ? $post['checksum'] : false;

        $this->checksum = $checksum;

        $this->getNotificationDebug()->write('checksum');

        $this->getNotificationDebug()->write($checksum);

        return $this->checksum;
    }

    /**
     * Check if the received CHECKSUM is OK
     * 
     * @return bool true on success
     */
    public function validate() {

        $hmac = $this->hmac($this->getEncoded(), $this->getSecret());

        $this->getNotificationDebug()->write('validate');

        $this->getNotificationDebug()->write($hmac);

        return $hmac === $this->getChecksum() ? true : false;
    }

    /**
     * Decode encoded post data
     * 
     * @return \Openapi\Epay\Types\ResponseType
     */
    public function decode() {

        $decode = base64_decode($this->getEncoded());

        $explode = explode(':', $decode);

        $data = [];

        $this->getNotificationDebug()->write('decode');

        foreach ($explode as $value) {

            $arr = explode('=', $value);

            if (empty($arr)) {
                continue;
            }

            $data[$arr[0]] = !empty($arr[1]) ? trim($arr[1]) : false;
        }

        $this->getNotificationDebug()->write($explode);

        return new ResponseType($data);
    }

}