<?php

namespace Openapi\Epay;

use Openapi\Epay\Debug\NotificationDebug;

/**
 * EpayComponent
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
abstract class EpayComponent {

    /**
     * @var string
     */
    private $date_format = 'd.m.Y H:i:s';

    /**
     * @var array
     */
    private $config = [];

    /**
     * @var array
     */
    private $env = [];

    /**
     * @var string
     */
    private $page = 'paylogin';

    /**
     * @var string
     */
    private $encoding = 'UTF-8';

    /**
     * @var string
     */
    private $debug;

    public function __construct($config = []) {

        $this->config = $config;


        $this->env = !empty($config['sandbox']) ?
                $config['development'] :
                $config['production'];
    }

    /**
     * Sets the value of payment type -  WEB login
     */
    public function paymentWeb() {

        $this->page = 'paylogin';
    }

    /**
     * Sets the value of payment type -  Credit card
     */
    public function paymentCreditCart() {

        $this->page = 'credit_paydirect';
    }

    /**
     * Sets the value of payment type - Payment deposit
     */
    public function paymentDeposit() {

        $this->page = 'paylogin';
    }

    /**
     * @return string Payment type - credit_paydirect, paylogin 
     */
    public function getPaymentType() {

        return $this->page;
    }

    /**
     * @return bool true if sandbox is enabled
     */
    public function isSandBox() {

        return !empty($this->config['sandbox']) ? true : false;
    }

    /**
     * @return array Environment configuration
     */
    public function getEnvironment() {

        return $this->env;
    }

    /**
     * @return string
     */
    public function getEncoding() {

        return !empty($this->config['encoding']) ? $this->config['encoding'] : $this->encoding;
    }

    /**
     * @return string
     */
    public function getReturnUrl() {

        return !empty($this->env['return_url']) ? $this->env['return_url'] : false;
    }

    /**
     * @return string
     */
    public function getCancelUrl() {

        return !empty($this->env['cancel_url']) ? $this->env['cancel_url'] : false;
    }

    /**
     * @return string
     */
    public function getEndpointUrl() {

        return !empty($this->env['endpoint_url']) ? $this->env['endpoint_url'] : false;
    }

    /**
     * @return string
     */
    public function getSecret() {

        return !empty($this->env['credentials']['secret']) ? $this->env['credentials']['secret'] : false;
    }

    /**
     * @return string
     */
    public function getClientEmail() {

        return !empty($this->env['credentials']['client_email']) ? $this->env['credentials']['client_email'] : false;
    }

    /**
     * @return string Payment expire time
     */
    public function getExpireTime() {

        return !empty($this->env['expire_after']) ?
                date($this->date_format, strtotime(date($this->date_format) . '+' . $this->env['expire_after'])) :
                date($this->date_format);
    }

    /**
     * @return string
     */
    public function getClientId() {

        return !empty($this->env['credentials']['client_id']) ? $this->env['credentials']['client_id'] : false;
    }

    /**
     * @param string $data base64encoded data
     * @param string $secret client secret
     * @param string $algorithm Algorithm (MD5|SHA1)
     * 
     * @return string
     */
    public function hmac($data, $secret, $algorithm = 'SHA1') {

        $algo = strtolower($algorithm);

        $pack = [
            'md5' => 'H32',
            'sha1' => 'H40'
        ];

        if (\strlen($secret) > 64) {
            $secret = \pack($pack[$algo], $algo($secret));
        }
        if (\strlen($secret) < 64) {
            $secret = str_pad($secret, 64, chr(0));
        }

        $ipad = \substr($secret, 0, 64) ^ str_repeat(chr(0x36), 64);

        $opad = \substr($secret, 0, 64) ^ str_repeat(chr(0x5C), 64);

        $al = ($algo($opad . \pack($pack[$algo], $algo($ipad . $data))));

        return $al;
    }

    /**
     * @param \Openapi\Epay\Debug\NotificationDebug $debug
     */
    public function setNotificationDebug(NotificationDebug $debug) {

        $this->debug = $debug;
    }

    /**
     * @return \Openapi\Epay\Debug\NotificationDebug
     */
    public function getNotificationDebug() {

        if (!empty($this->debug)) {

            return $this->debug;
        }

        $this->debug = new NotificationDebug();

        return $this->debug;
    }

}
