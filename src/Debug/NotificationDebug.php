<?php

namespace Openapi\Epay\Debug;

/**
 * NotificationDebug
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class NotificationDebug {

    /**
     * @var string
     */
    private $debug_folder = false;

    /**
     * Sets the debug folder
     * 
     * @param string $debug_folder Full path to debug folder
     */
    public function setDebugFolder($debug_folder) {

        if (!file_exists($debug_folder)) {

            mkdir($debug_folder, 0755, true);
        }

        $this->debug_folder = $debug_folder;
    }

    /**
     * @return string
     */
    public function getDebugFolder() {

        return $this->debug_folder;
    }

    /**
     * @param mixed $message Message or Array to write. Array is 
     */
    public function write($message = '') {

        if (empty($this->debug_folder) || empty($message)) {

            return false;
        }

        $folder = chop($this->debug_folder, '/') . '/';

        if (is_array($message)) {

            file_put_contents($folder . 'notification.log', '[data : ' . date('Y-m-d H:i:s') . ']' . PHP_EOL, FILE_APPEND);

            $data_array = array_keys($message);

            array_walk($data_array, function(&$value, $key, $values) {

                $value = !empty($values[$key]) ? $value . '=' . $values[$key] : false;
            }, array_values($message));

            $imploded = implode(PHP_EOL, array_filter($data_array));

            file_put_contents($folder . 'notification.log', $imploded . PHP_EOL, FILE_APPEND);

            return true;
        }

        file_put_contents($folder . 'notification.log', '[' . date('Y-m-d H:i:s') . '] ' . $message . PHP_EOL, FILE_APPEND);
    }

}
