<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Max-Age: 1728000');

/**
 * Basic example response from ePay
 */
require_once '../vendor/autoload.php';

use Openapi\Epay\Notifications\EpayNotification;

//load config file
$config = file_get_contents('config.json');

//get the credentials
$credentials = json_decode($config, true);

//load class receiving the notifications
$epay = new EpayNotification($credentials);


//check if the checksum is not valid
if (!$epay->validate()) {

    //@todo write log
}

//decode the encoded invoice data from ePay
$invoice = $epay->decode();

//get the invoice from ePay post data 
$invoice_number = $invoice->getInvoiceNumber();

if (empty($invoice_number)) {

    echo $invoice->returnError('Invoice not found');

    exit;
}

//you can manually check the status 
$status = $invoice->getStatus();

//you can use short method to get the Invoice status

$invoice->isPaid();

$invoice->isCanceled();

$invoice->isExpired();


//return response to ePay
echo $invoice->returnSuccess();

exit;
