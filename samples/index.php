<?php
/**
 * Basic example create invoice and pay 
 */
require_once '../vendor/autoload.php';

use Openapi\Epay\Payments\WebPayment;
use Openapi\Epay\Types\InvoiceType;

//load config file
$config = file_get_contents('config.json');

//get the credentials
$credentials = json_decode($config, true);

//load webpayments class
$epay = new WebPayment($credentials);

//create new invoice type - new instance with parameters
$invoice = new InvoiceType([
    'amount' => '120.80',
    'description' => 'Test invoice'
        ]);

//create new invoice type without parameters
$invoice = new InvoiceType();

//amount - float
$invoice->amount = '120.80';

//description 100 chars max
$invoice->description = 'Test invoice';

//if invoice number is not set, it will be automatically generated
$invoice->invoice_number = sprintf("%.0f", rand(0, 999999));

//create the invoice
$epay->generateInvoice($invoice);

//
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>ePay Basic example</title>
        <link rel="icon" href="img/mdb-favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.14.0/css/mdb.min.css" rel="stylesheet">
    </head>
    <body>
        <main>
            <div class="main-wrapper">
                <div class="container">
                    <section class="my-5">
                        <div class="row">
                            <h4>Demo Invoice</h4>
                            <hr />
                        </div>
                        <!-- set endpoint URL to send the post data-->
                        <form action="<?= $epay->getEndpointUrl() ?>" method=POST>
                            <div class="row">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Order</th>
                                            <th>Invoice</th>
                                            <th>Price</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?= $invoice->description ?></td>
                                            <td><?= $invoice->invoice_number ?></td>
                                            <td><?= $invoice->amount ?> <?= $invoice->currency ?></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <hr />
                                <!--generate hidden input fields required from ePay-->
                                <input type=hidden name=PAGE value="<?= $epay->getPaymentType() ?>">
                                <input type=hidden name=ENCODED value="<?= $epay->getEncoded() ?>">
                                <input type=hidden name=CHECKSUM value="<?= $epay->getChecksum() ?>">
                                <input type=hidden name=URL_OK value="<?= $epay->getReturnUrl() ?>">
                                <input type=hidden name=URL_CANCEL value="<?= $epay->getCancelUrl() ?>">
                                <button type="submit" class="btn btn-primary btn-rounded btn-sm waves-effect waves-light">Checkout</button>
                            </div>
                        </form>    
                    </section>
                </div>
            </div>
        </main>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.14.0/js/mdb.min.js"></script>
    </body>
</html>