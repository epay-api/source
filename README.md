# library for working with ePay.bg and EasyPay communication package for merchants via API

*  Example config

````
{
    "sandbox": 1,
    "encoding": "UTF-8",
    "production": {
        "expire_after": "6 hours",
        "debug": false,
        "credentials": {
            "secret": "secret_key_production",
            "client_id": "merchant_client_ident_number",
            "client_email": "merchant_email"
        },
        "endpoint_url": "https://www.epay.bg/",
        "return_url": "https://yourdomain.com/thank-you.php",
        "cancel_url": "https://yourdomain.com/cancel.php"
    },
    "development": {
        "expire_after": "24 hours",
        "debug": true,
        "credentials": {
            "secret": "secret_key_demo",
            "client_id": "merchant_client_ident_number_demo",
            "client_email": "merchant_email"
        },
        "endpoint_url": "https://devep2.datamax.bg/ep2/epay2_demo/",
        "return_url": "https://staging.yourdomain.com/thank-you.php",
        "cancel_url": "https://staging.yourdomain.com/cancel.php"
    }
}
````

*  Use in your project

````
require_once 'vendor/autoload.php';

use Openapi\Epay\Payments\WebPayment;
use Openapi\Epay\Types\InvoiceType;

````

*  Load WebPayment class

````

$config = file_get_contents('config.json');

$credentials = json_decode($config, true);

$epay = new WebPayment($credentials);

$epay->paymentWeb();

````

*  Create new Invoice type - instance of class with parameters

````
$invoice = new InvoiceType([
    'amount' => '120.80',
    'description' => 'Test Invoice',
    'invoice_number' => sprintf("%.0f", rand(0, 999999)) //if invoice number is not set, it will be automatically generated
        ]);
        
````

*  Or Create new Invoice type - instance of class only

````
$invoice = new InvoiceType();

$invoice->amount = '120.80';

$invoice->description = 'Test Invoice';

//if invoice number is not set, it will be automatically generated
$invoice->invoice_number = sprintf("%.0f", rand(0, 999999));


````

*  Generate Invoice

````

$epay->generateInvoice($invoice);

````

*  Generating the webform where send data to ePay endpoint URL (example from demo page at https://epay.openapiservices.com/ )


````
<form action="<?= $epay->getEndpointUrl() ?>" method=POST>
    <div class="row">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Order</th>
                    <th>Invoice</th>
                    <th>Price</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?= $invoice->description ?></td>
                    <td><?= $invoice->invoice_number ?></td>
                    <td><?= $invoice->amount ?> <?= $invoice->currency ?></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="row">
    
        <hr />
        <!--generate hidden input fields required from ePay-->
        <input type=hidden name=PAGE value="<?= $epay->getPaymentType() ?>">
        
        <input type=hidden name=ENCODED value="<?= $epay->getEncoded() ?>">
        
        <input type=hidden name=CHECKSUM value="<?= $epay->getChecksum() ?>">
        
        <input type=hidden name=URL_OK value="<?= $epay->getReturnUrl() ?>">
        
        <input type=hidden name=URL_CANCEL value="<?= $epay->getCancelUrl() ?>">
        
        <button type="submit" class="btn btn-primary btn-rounded btn-sm waves-effect waves-light">Checkout</button>
        
    </div>
</form>

````







